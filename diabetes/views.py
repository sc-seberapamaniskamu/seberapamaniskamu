from django.shortcuts import render
from django.http import HttpResponseRedirect
from .obesity import *
from .bayesian2 import *



def information(request):
    return render(request, 'Information.html')

def anyIdea(request):
    return render(request, 'anyIdea.html')

def treatment(request):
    return render(request, 'treatment.html')
    
def about(request):
    return render(request, 'about.html')
	
def result(request):
	if request.method == 'POST':
		gender = request.POST['gender']
		smoker = request.POST['smoker']
		age = request.POST['usia']
		tinggi = request.POST['tinggi']
		berat = request.POST['berat']

		givenSet = set()
		

		if (gender != "Unknown"):
			if (gender == "Pria"):
				givenSet.add("G")
			else:
				givenSet.add("nG")
	
		
		if (smoker != "Unknown"):
			if (smoker == "Ya"):
				givenSet.add("S")
			else:
				givenSet.add("nS")

		if(tinggi != "" and berat != ""):
			if(isObesity(int(tinggi),int(berat))):
				givenSet.add("O")
			else:
				givenSet.add("nO")


		if (age != ""):
			if (int(age) < 44):
				givenSet.add("1")
			elif (int(age) >= 44 and int(age) < 65 ):
				givenSet.add("2")
			else:
				givenSet.add("3")
		
		# print(givenSet)
		result = inference("D",givenSet)
		# print(result)
		# result = hitung1(smokerSym, genderSym, obesitySym,ageSym)
		finalResult = round(result * 100,2)
		return render(request, 'Result.html', {"result" : finalResult})

