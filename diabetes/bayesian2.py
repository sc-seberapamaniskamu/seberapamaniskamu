# Dictionary buat data untuk setiap node
smoker = {"T": 0.243, "F": 0.757, "X":1}
gender = {"TT": 0.8,"FT":0.45, "TF": 0.2, "FF": 0.55, "X":1}
obesitas = {"T": 0.07,"F":0.93, "X":1}
age = {"1": 0.714, "2": 0.2209, "3": 0.0651, "X":1}
diabetes = {"TTT1":0.727, "TTT2":0.805,"TTT3":0.79,
            "TTF1":0.127, "TTF2":0.205,"TTF3":0.19,
            "TFT1":0.734, "TFT2":0.812,"TFT3":0.797,
            "TFF1":0.134, "TFF2":0.212,"TFF3":0.197,
            "FTT1":1-0.727, "FTT2":1-0.805,"FTT3":1-0.79,
            "FTF1":1-0.127, "FTF2":1-0.205,"FTF3":1-0.19,
            "FFT1":1-0.734, "FFT2":1-0.812,"FFT3":1-0.797,
            "FFF1":1-0.134, "FFF2":1-0.212,"FFF3":1-0.197,
            "X":1}

nodes = {"A","G","O","S","D"}
def getParents(child):
    if(child == "A"):
        return {}
    elif (child == "G"):
        return {"S"}
    elif (child == "O"):
        return {}
    elif (child == "S"):
        return {}
    elif (child == "D"):
        return {"G","O","A"}

def inference(asked, given): # "asked" variable is a string and "given" variable is a set of strings
    result = 0
    
    if(asked[0]=="n"):
        newAsked = asked[1]
    else:
        newAsked = asked
    
    par = set(getParents(newAsked))
    for i in par:
        par = par.union(getParents(i))
    
    newGiven = set()
    for i in given:
        if(i[0] == "n"):
            newGiven.add(i[1])
        elif (i in "123"):
            newGiven.add("A")
        else:
            newGiven.add(i)
    unknown = par-newGiven-set(newAsked)

    if(asked[0] == "n"):
        boolAsked = "F"
    elif (asked in "123"):
        boolAsked = asked
    else:
        boolAsked = "T"


    if ("A" in unknown):
        loopA = ["1","2","3"]
    elif ("1" in asked or "2" in asked or "3" in asked):
        loopA = [boolAsked]
        boolA = loopA[0]
    else:
        loopA = ["X"]
        if ("1" in given):
            boolA = "1"
        elif ("2" in given):
            boolA = "2"
        elif ("3" in given):
            boolA = "3"

    for a in loopA:
        if ("O" in unknown):
            loopO = ["T","F"]
        elif ("O" in asked):
            loopO = [boolAsked]
            boolO = loopO[0]
        else:
            loopO = ["X"]
            if ("nO" in given):
                boolO = "F"
            elif ("O" in given):
                boolO = "T"
            
        
        for o in loopO:
            if ("S" in unknown):
                loopS = ["T","F"]
            elif ("S" in asked):
                loopS = [boolAsked]
                boolS = loopS[0]
            else:
                loopS = ["X"]
                if ("nS" in given):
                    boolS = "F"
                elif ("S" in given):
                    boolS = "T"

            
            for s in loopS:
                
                if ("G" in unknown):
                    if("S" in unknown):
                        loopG = [s+"T",s+"F"]
                    else:
                        loopG = [boolS+"T",boolS+"F"]

                elif ("G" in asked):
                    if("S" in unknown):
                        loopG = [s+boolAsked]
                        boolG = loopG[0][0]
                        
                    else:
                        loopG = [boolS+boolAsked]
                        boolG = loopG[0][0]
                else:
                    loopG = ["X"]
                    if ("nG" in given):
                        boolG = "F"
                    elif ("G" in given):
                        boolG = "T"

                for g in loopG:
                    if ("D" in unknown):
                        if ("G" in unknown and "O" in unknown and "A" in unknown):
                            loopD = ["T"+g[1]+o+a,"F"+g[1]+o+a]
                        elif("G" in unknown and "O" in unknown):
                            loopD = ["T"+g[1]+o+boolA,"F"+g[1]+o+boolA]
                        elif("G" in unknown and "A" in unknown):
                            loopD = ["T"+g[1]+boolO+a,"F"+g[1]+boolO+a]
                        elif("O" in unknown and "A" in unknown):
                                loopD = ["T"+boolG+o+a,"F"+boolG+o+a]
                        elif("G" in unknown):
                            loopD = ["T"+g[1]+boolO+boolA,"F"+g[1]+boolO+boolA]
                        elif("O" in unknown):
                            loopD = ["T"+boolG+o+boolA,"F"+boolG+o+boolA]
                        elif("A" in unknown):
                            loopD = ["T"+boolG+boolO+a,"F"+boolG+boolO+a]
                        else:
                            loopD = ["T"+boolG+boolO+boolA,"F"+boolG+boolO+boolA]
                        
                    elif ("D" in asked):  
                        if ("G" in unknown and "O" in unknown and "A" in unknown):
                            loopD = [boolAsked+g[1]+o+a]
                        elif("G" in unknown and "O" in unknown):
                            loopD = [boolAsked+g[1]+o+boolA]
                        elif("G" in unknown and "A" in unknown):
                            loopD = [boolAsked+g[1]+boolO+a]
                        elif("O" in unknown and "A" in unknown):
                                loopD = [boolAsked+boolG+o+a]
                        elif("G" in unknown):
                            loopD = [boolAsked+g[1]+boolO+boolA]
                        elif("O" in unknown):
                            loopD = [boolAsked+boolG+o+boolA]
                        elif("A" in unknown):
                            loopD = [boolAsked+boolG+boolO+a]
                        else:
                            loopD = [boolAsked+boolG+boolO+boolA]

                    else:
                        loopD = ["X"]
                    
                    for d in loopD:
                        ageVal = age.get(a)
                        obesitasVal = obesitas.get(o)
                        genderVal = gender.get(g)
                        smokerVal = smoker.get(s)
                        diabetesVal = diabetes.get(d)

                        result = result + ageVal*obesitasVal*genderVal*smokerVal*diabetesVal
    return result
                        
        
# print(inference("D",{"nS","1","nO","nG"}))
# print(inference("D", {"nS", 'n0', '1', 'nG'}))
#{'nS', 'n0', '1', 'nG'}