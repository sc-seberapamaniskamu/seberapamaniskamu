# Dictionary buat data untuk setiap node
smoker = {"T": 0.243, "F": 0.757}
gender = {"TT": 0.8,"FT":0.45, "TF": 0.2, "FF": 0.55}
obesitas = {"T": 0.07,"F":0.93}
age = {"1": 0.714, "2": 0.2209, "3": 0.0651}
diabetes = {"TT1":0.727, "TT2":0.805,"TT3":0.79,
            "TF1":0.127, "TF2":0.205,"TF3":0.19,
            "FT1":0.734, "FT2":0.812,"FT3":0.797,
            "FF1":0.134, "FF2":0.212,"FF3":0.197}




### String atribute
nodes = [smoker, gender, obesitas, age]

### Arrays of loop
#all unknown
status0 = ["TTT1","TTT2","TTT3","TTF1","TTF2","TTF3","TFT1","TFT2","TFT3","TFF1","TFF2","TFF3", "FTT1","FTT2","FTT3","FTF1","FTF2","FTF3","FFT1","FFT2","FFT3","FFF1","FFF2","FFF3"] 
# age and two between smoking, obesity, and gender is Unknown
status1 = ["TT1","TT2","TT3","TF1","TF2","TF3","FT1","FT2","FT3","FF1","FF2","FF3"] 
# smoking, gender, and obesity is Unknown
status2 = ["TTT","TTF","TFT","TFF","FTT","FTF","FFT","FFF"] 
# age and between smoking, gender, or obesity is Unknown
status3 = ["T1","T2","T3","F1","F2","F3"] 
# two of smoking, gender, or obesity is Unknown
status4 = ["TT","TF","FT","FF"]
# either one of smoking, gender, or obesity is Unknown
status5 = ["T","F"]
# age is Unknown
status6 = ["1","2","3"]

# method inference 

def inference(S,G,O,A):

    #inference formula P(D|S,G,O,A) = P(D,S,G,O,A)

    result = 0


    smokerval = smoker.get(S)
    genderVal = gender.get(S+G)
    obesitasVal = obesitas.get(O)
    ageVal = age.get(A)
    diabetesVal = diabetes.get(G+O+A)
    
    
    result = diabetesVal * ageVal * obesitasVal * genderVal * smokerval 
    return result

    


# method hitung

def hitung1(smoker, gender, obesity, age):
    hitung = 0
    if ((smoker+gender+obesity+age).count("U") == 4):
        for i in status0:
            hitung = hitung + inference(i[0],i[1],i[2],i[3])
    elif ((smoker+gender+obesity+age).count("U") == 3):
        if (age == "U"):                
            if (smoker != "U"):
                for i in status1:
                    hitung = hitung + inference(smoker,i[0],i[1],i[2])
            elif (gender != "U"):
                for i in status1:
                    hitung = hitung + inference(i[0],gender,i[1],i[2])
            else:
                for i in status1:
                    hitung = hitung + inference(i[0],i[1],obesity,i[2])
        else:
            for i in status2:
                hitung = hitung + inference(i[0],i[1],i[2],age)
    elif ((smoker+gender+obesity+age).count("U") == 2):
        if (age == "U"):
            if (smoker == "U"):
                for i in status3:
                    hitung = hitung + inference(i[0],gender,obesity,i[1])
            elif (gender == "U"):
                for i in status3:
                    hitung = hitung + inference(smoker,i[0],obesity,i[1])
            elif (obesity == "U"):
                for i in status3:
                    hitung = hitung + inference(smoker,gender,i[0],i[1])
        else:
            if (smoker != "U"):
                for i in status4:
                    hitung = hitung + inference(smoker,i[0],i[1],age)
            elif (gender != "U"):
                for i in status4:
                    hitung = hitung + inference(i[0],gender,i[1],age)
            elif (obesity != "U"):
                for i in status4:
                    hitung = hitung + inference(i[0],i[1],obesity,age)
    elif (smoker == "U"):
        for i in status5:
            hitung = hitung + inference(i[0],gender,obesity,age)
    elif (gender == "U"):
        for i in status5:
            hitung = hitung + inference(smoker,i[0],obesity,age)
    elif (obesity == "U"):
        for i in status5:
            hitung = hitung + inference(smoker,gender,i[0],age)
    elif (age == "U"):
        for i in status6:
            hitung = hitung + inference(smoker,gender,obesity,i[0])
    else:
        hitung = hitung + inference(smoker,gender,obesity,age)
    
    return hitung

"""
def hitung(smoking, gender, obesity, age):
    hitung = 0
    # all unknown
    if (smoking == "U" and age == "U" and obesity == "U" and gender == "U"):
        for i in status0:
            hitung = hitung + inference(i[0],i[1],i[2],i[3])
    # only age is known
    elif (smoking == "U" and age == "U" and obesity == "U"):
        for i in status3:
            hitung = hitung + inference(i[0],i[1],i[2],age)
    # age and 1 other is known but the rest is unknwon
    elif (age == "U" and obesity == "U" and smoking == "U" or age == "U" and gender == "U" and smoking == "U"):
    ### kebawah masih ga jelas
    elif (age == "U" and obesity == "U" and gender == "U"):
        for i in status1:
            hitung = hitung + inference(i[0],i[1],i[2])
    elif (age == "U" and obesity == "U" or age == "U" and gender == "U"):
        if(obesity == "U"):
            for i in status2:
                hitung = hitung + inference(gender, i[0], i[1])
        else:
            for i in status2:
                hitung = hitung + inference(i[0],obesity,i[1])

    elif (obesity == "U" and gender == "U"):
        for i in status3:
            hitung = hitung + inference(i[0], i[1], age)

    elif (obesity == "U" or gender == "U"):
        if( obesity == "U"):
            for i in status4:
                hitung = hitung + inference(gender, i, age)
        else:
            for i in status4:
                hitung = hitung+ inference(i, obesity, age)
    elif (age == "U"):
        for i in status5:
            hitung = hitung+inference(gender, obesity, i)
    else:
        hitung = inference(gender, obesity, age)

    return hitung

def hitung(gender, obesity, age):
    hitung = 0

    if (age == "U" and obesity == "U" and gender == "U"):
        for i in status1:
            hitung = hitung + inference(i[0],i[1],i[2])
    elif (age == "U" and obesity == "U" or age == "U" and gender == "U"):
        if(obesity == "U"):
            for i in status2:
                hitung = hitung + inference(gender, i[0], i[1])
        else:
            for i in status2:
                hitung = hitung + inference(i[0],obesity,i[1])

    elif (obesity == "U" and gender == "U"):
        for i in status3:
            hitung = hitung + inference(i[0], i[1], age)

    elif (obesity == "U" or gender == "U"):
        if( obesity == "U"):
            for i in status4:
                hitung = hitung + inference(gender, i, age)
        else:
            for i in status4:
                hitung = hitung+ inference(i, obesity, age)
    elif (age == "U"):
        for i in status5:
            hitung = hitung+inference(gender, obesity, i)
    else:
        hitung = inference(gender, obesity, age)

    return hitung
"""