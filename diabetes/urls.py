from django.urls import path
from diabetes import views


app_name = 'diabetes'

urlpatterns = [
    path('information', views.information, name = "information"),
    path('anyIdea', views.anyIdea, name = "anyIdea"),
    path('result', views.result, name = "result"),
    path('about', views.about, name = "about"),
    path('treatment', views.treatment, name = "treatment")
]