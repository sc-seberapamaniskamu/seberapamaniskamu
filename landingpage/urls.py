from django.urls import path
from landingpage import views

app_name = 'landing'

urlpatterns = [
    path('', views.index, name = "landingpage")
]